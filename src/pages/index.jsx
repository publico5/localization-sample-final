import { Fragment, useEffect, useState } from "react";
import axios from "axios";
import styles from "../styles/pages/Home.module.scss";
import { handleDate } from "../helpers/helpers";

import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { useTranslation } from "next-i18next";

export default function Home({ data }) {
  // stages for the categories/types
  const [stage, setStage] = useState(0);
  const [results, setResults] = useState([]);
  const [index, setIndex] = useState(0);

  const { t } = useTranslation("home");

  // handle category button click -------
  const handleStoryType = async (type) => {
    // will be pluged into a backend

    let res = "";

    if (
      type === "in-memory" ||
      type === "missing" ||
      type === "heroes" ||
      type === "our-lives-now" ||
      type === "animal"
    ) {
      res = type;
    }

    if (type === "in-memory") {
      setStage(0);
      res = type;
      // call api for in-memory type ---
    } else if (type === "missing") {
      setStage(1);
      res = type;
      // call api for missing type ---
    } else if (type === "heroes") {
      setStage(2);
      res = type;
      //call api for heroes type ---
    } else if (type === "our-lives-now") {
      setStage(3);
      res = type;
      // call api for our-lives-now type ---
    } else if (type === "animal") {
      setStage(4);
      res = type;
      // call api for animals type ---
    }

    try {
      //
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    getStoriesAndContributions();
  }, []);

  const getStoriesAndContributions = async () => {
    const [{ data: stories }, { data: contributions }] = await Promise.all([
      axios.get(`/api/stories/category/in-memory`),
      axios.get(`/api/contributions/category/in-memory`),
      // axios.get(`/api/stories`),
      // axios.get(`/api/contributions`),
    ]);

    setResults([...stories, ...contributions]);
  };

  useEffect(() => {
    if (results) {
      const timer = setInterval(() => {
        setIndex((current) =>
          current == results.length - 1 ? 0 : current + 1
        ); // <-- Change this line!
        // setIndex((current) => (current == stories.length ? 0 : current + 1)); // <-- Change this line!
      }, 10000);
      return () => {
        clearInterval(timer);
      };
    }
  }, [results]);
  // handle active classes
  const buttonClasses = {
    inMemory: stage === 0 ? styles.active : "",
    missing: stage === 1 ? styles.active : "",
    heroes: stage === 2 ? styles.active : "",
    ourLivesNow: stage === 3 ? styles.active : "",
    animals: stage === 4 ? styles.active : "",
  };

  return (
    <>
      <section className={styles.hero}>
        <h1>test</h1>

        {/* categories | types ------------- */}
        <div className={styles.categories}>
          <button
            className={buttonClasses.inMemory}
            onClick={() => handleStoryType("in-memory")}
          >
            {t("in-memory")}
          </button>
          <button
            className={buttonClasses.missing}
            onClick={() => handleStoryType("missing")}
          >
            {t("missing")}
          </button>
          <button
            className={buttonClasses.heroes}
            onClick={() => handleStoryType("heroes")}
          >
            {t("heroes")}
          </button>
          <button
            className={buttonClasses.ourLivesNow}
            onClick={() => handleStoryType("our-lives-now")}
          >
            {t("our-lives-now")}
          </button>
          <button
            className={buttonClasses.animals}
            onClick={() => handleStoryType("animal")}
          >
            {t("animals")}
          </button>
        </div>
      </section>

      {/* main section ------------- */}
      <main className={styles.main}>
        <div className={styles.mainWrapper}>
          <div className={styles.card}>
            {results[index] && results[index].image && (
              <img src={results[index].image.Location} alt={"an image"} />
            )}
            <div className={styles.textContainer}>
              {results[index] && (
                <h3>
                  {results[index].firstName} {results[index].lastName}
                </h3>
              )}
              {results[index] && (
                <p className={styles.dob}>
                  {t("date-of-birth")}: {handleDate(results[index].dob)}
                </p>
              )}
              {results[index] && (
                <p className={styles.story}>{results[index].story}</p>
              )}
            </div>
          </div>
        </div>
      </main>

      {/* 
          This regex match splits the story component into parts of equal length
          {data[index].story.match(/[\s\S]{1,280}/g).map(part => (
            <p key={part.substring(0, 16)}>
              {part}
            </p>
          ))} 
      */}
    </>
  );
}

export async function getStaticProps({ locale }) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ["common", "home", "nav"])), // Will be passed to the page component as props
      // data: [
      //   {
      //     name: "John Brown",
      //     dob: "06-16-1987",
      //     story:
      //       "This is the first element, consectetur adipiscing elit. Quisque cursus odio nibh, et vehicula neque sollicitudin vel. Aliquam erat volutpat. Sed efficitur libero vitae ante fermentum ultricies. Donec fringilla diam ac magna viverra placerat. Duis vel magna dui. Suspendisse a sodales ligula. Vestibulum justo risus, feugiat et nulla nec, varius vestibulum nisl. Sed efficitur libero vitae ante fermentum ultricies. Donec fringilla diam ac magna viverra placerat.",
      //     image: "/family.jpg",
      //     alt: "family of four walking in the street",
      //   },
      //   {
      //     name: "Louise Smith",
      //     dob: "11-22-1990",
      //     story:
      //       "This is the second element, consectetur adipiscing elit. Quisque cursus odio nibh, et vehicula neque sollicitudin vel. Aliquam erat volutpat. Sed efficitur libero vitae ante fermentum ultricies. Duis vel magna dui. Suspendisse a sodales ligula. Vestibulum justo risus, feugiat et nulla nec, varius vestibulum nisl. Sed efficitur libero vitae ante fermentum ultricies. Donec fringilla diam ac magna viverra placerat. Donec fringilla diam ac magna viverra placerat.",
      //     image: "/woman.jpg",
      //     alt: "woman wearing a yellow shirt with long sleeves",
      //   },
      //   {
      //     name: "Sabo",
      //     dob: "02-27-2016",
      //     story:
      //       "Here is the thrid element, consectetur adipiscing elit. Quisque cursus odio nibh, et vehicula neque sollicitudin vel. Sed efficitur libero vitae ante fermentum ultricies. Donec fringilla diam ac magna viverra placerat. Aliquam erat volutpat. Duis vel magna dui. Suspendisse a sodales ligula. Vestibulum justo risus, feugiat et nulla nec, varius vestibulum nisl. Sed efficitur libero vitae ante fermentum ultricies. Donec fringilla diam ac magna viverra placerat.",
      //     image: "/dog.jpg",
      //     alt: "dog with golden fur",
      //   },
      // ],
    },
  };
}

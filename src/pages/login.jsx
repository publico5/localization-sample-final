import { useState } from "react";
import PasswordInput from "../components/PasswordInput";
import styles from "../styles/pages/LoginSignup.module.scss";
import atoms from "../styles/atoms.module.scss";
import axios from "axios";
import { authenticate } from "../helpers/helpers";
import { toast } from "react-toastify";
import { useRouter } from "next/router";
import Link from "next/link";
//redux
import { useDispatch, useSelector } from "react-redux";
import { signin } from "../redux/features/auth";
// translation
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { useTranslation } from "next-i18next";

export default function Login() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const { t } = useTranslation("common");

  // router
  const router = useRouter();
  // redux
  const auth = useSelector((state) => state.auth.value);
  const dispatch = useDispatch();

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      const { data } = await axios.post(`/api/login`, {
        email,
        password,
      });
      // set cookie and localstorage
      if (data) {
        // set cookie and localstorage
        authenticate(data);
        // // add to state
        dispatch(signin(data));
        // router.push("/");
        // console.log(data.user.role);
        if (data.user.role.includes("admin")) router.push("/admin");
        else router.push("/");
        // if (res.data.user.role === "learner") navigate("/dashboard");
        // if (res.data.user.role === "teacher") navigate("/dashboard/seller");
      }
    } catch (err) {
      console.log(err.response.data);
      toast(err.response.data);
    }
  };

  return (
    <div className={styles.container}>
      <div className={styles.formWrapper}>
        <h1>{t("login")}</h1>
        <form
          className={atoms.form}
          style={{ justifyContent: "space-between", flexGrow: 1 }}
          onSubmit={handleSubmit}
        >
          <div className={styles.inputsWrapper}>
            <input
              className={atoms.input}
              type="email"
              name="email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
              placeholder={t("email")}
              required
            />
            <PasswordInput
              minLength={8}
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            />
            <Link href="/forgot-password">
              <p style={{ color: "red", cursor: "pointer" }}>
                {t("forgot-password")}
              </p>
            </Link>
          </div>
          <button
            className={`${atoms.button} ${styles.loginButton}`}
            type="submit"
          >
            {t("login")}
          </button>
        </form>
      </div>
    </div>
  );
}

export async function getStaticProps({ locale }) {
  return {
    props: { ...(await serverSideTranslations(locale, ["common", "nav"])) },
  };
}

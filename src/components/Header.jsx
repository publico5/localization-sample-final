import Link from "next/link";
import { useEffect, useState } from "react";
import styles from "../styles/components/Header.module.scss";
import SideNav from "./SideNav";
import { signout } from "../helpers/helpers";
import { useRouter } from "next/router";
// redux
import { useSelector, useDispatch } from "react-redux";
import { logout } from "../redux/features/auth";

// other imports
import { useTranslation } from "next-i18next";

export default function Header() {
  const [showNav, setShowNav] = useState(false);
  const [isAdmin, setIsAdmin] = useState(false);
  const [isSuperAdmin, setIsSuperAdmin] = useState(false);

  const auth = useSelector((state) => state.auth.value);
  const dispatch = useDispatch();
  const router = useRouter();
  const { t } = useTranslation("nav");

  useEffect(() => {
    auth && setIsAdmin(auth.user.role.includes("admin") ? true : false);
    auth &&
      setIsSuperAdmin(auth.user.role.includes("superAdmin") ? true : false);
  }, [auth]);

  const toggleNav = () => setShowNav(!showNav);

  const handleLogout = () => {
    signout();
    dispatch(logout());
    router.push("/");
  };

  const handleLocaleChange = (event) => {
    const value = event.target.value;
    router.push(router.route, router.asPath, { locale: value });

    // If no argument is passed, it will use `common.json`
  };

  return (
    <header className={styles.container}>
      <nav className={`${styles.nav} ${showNav ? styles.show : ""}`}>
        <div className={styles.navLeft}>
          <Link href="/">
            <a>{t("home")}</a>
          </Link>
          {/* <Link href="/about-us">
            <a>{t("about-us")}</a>
          </Link>
          <Link href="/contact-us">
            <a>{t("contact-us")}</a>
          </Link> */}
          <select onChange={handleLocaleChange} value={router.locale}>
            <option value="en">English</option>
            <option value="fr">francais</option>
          </select>
          {auth && (isAdmin || isSuperAdmin) && (
            <Link href="/admin">
              <a style={{ color: "red" }}>{t("admin")}</a>
            </Link>
          )}
        </div>
        {auth ? (
          <span>
            <button className={styles.login} onClick={handleLogout}>
              Logout
            </button>
          </span>
        ) : (
          <Link href="/login">
            <button className={styles.login}>{t("login")}</button>
          </Link>
        )}
      </nav>

      {showNav ? (
        // close Icon
        <button className={styles.closeIcon} onClick={toggleNav}>
          &times;
        </button>
      ) : (
        // hamburger
        <button
          aria-label="Menu button"
          className={styles.menuIcon}
          onClick={toggleNav}
        >
          <div aria-hidden />
          <div aria-hidden />
          <div aria-hidden />
        </button>
      )}

      {/* backdrop of sidenav */}
      {showNav && (
        <div
          className={styles.backdrop}
          onClick={() => setShowNav(false)}
        ></div>
      )}
      {/* sidenav */}
      <SideNav showNav={showNav} setShowNav={setShowNav} />
    </header>
  );
}

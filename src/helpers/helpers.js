import cookie from "js-cookie";

export const authenticate = (response) => {
  setCookie("token", response.token);
  setLocalStorage("user", response.user);
  // next();
};

// set in cookie
// key is the name of the cookie -- token whatever  'token' value ' something'
export const setCookie = (key, value) => {
  //need window if it is not there --- then we cannot continue
  if (window !== "undefined") {
    cookie.set(key, value, {
      expires: 1, // here expires in 1 day
    });
  }
};
// remove from cookie
export const removeCookie = (key) => {
  if (window !== "undefined") {
    cookie.remove(key, {
      expires: 1,
    });
  }
};
// get from cookie such as stored token
// will be useful when we need to make request to server with token
export const getCookie = (key) => {
  if (window !== "undefined") {
    return cookie.get(key);
  }
};
// set in localstorage
export const setLocalStorage = (key, value) => {
  if (window !== "undefined") {
    localStorage.setItem(key, JSON.stringify(value));
  }
};
// remove from localstorage
export const removeLocalStorage = (key) => {
  if (window !== "undefined") {
    localStorage.removeItem(key);
  }
};
// authenticate user by passing data to cookie and localstorage during signin

export const getUser = () => {
  if (window !== "undefined") {
    return JSON.parse(localStorage.getItem("user"));
  }
};

// access user info from localstorage
export const isAuth = () => {
  if (window !== "undefined") {
    const cookieChecked = getCookie("token");
    if (cookieChecked) {
      if (localStorage.getItem("user")) {
        let auth = {
          user: getUser(),
          token: getCookie("token"),
        };
        // make sure a JS object returned
        return auth;
      } else {
        // no authenticated method
        return false;
      }
    }
  }
};

export const signout = () => {
  removeCookie("token");
  removeLocalStorage("user");
  //  next();
};

export const updateUser = (response, next) => {
  console.log("UPDATE USER IN LOCALSTORAGE HELPERS", response);
  if (typeof window !== "undefined") {
    let auth = JSON.parse(localStorage.getItem("user"));
    auth = response.data;
    localStorage.setItem("user", JSON.stringify(auth));
  }
  next();
};

export const handleDate = (i) => {
  let dateObj = new Date(i);
 // let month = dateObj.getUTCMonth() + 1; //months from 1-12
  let month = ("0" + (dateObj.getMonth() + 1)).slice(-2)
  let day = ("0" + (dateObj.getUTCDate())).slice(-2)
  let year = dateObj.getUTCFullYear();

  let newDate = year + "-" + month + "-" + day;

  return newDate;
};

import { createSlice } from "@reduxjs/toolkit";
// import { isAuth } from "../../helpers/helpers";
// let userState;

// if (typeof window !== "undefined") {
//   // if (window.localStorage.getItem("user")) {
//   //userState = JSON.parse(window.localStorage.getItem("user"));
//   // userState = isAuth();
//   userState = null;
// } else {
//   userState = null; // {}
// }
//}

// userState = isAuth();

const initialStateValue = null;

export const authSlice = createSlice({
  name: "auth",
  initialState: { value: initialStateValue },
  reducers: {
    signin: (state, action) => {
      state.value = action.payload;
    },

    logout: (state) => {
      state.value = null;
    },
  },
});

export const { signin, logout } = authSlice.actions;

export default authSlice.reducer;
